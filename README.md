#Развёртывание приложения

##Development
phing build -f ./build/dev/build.xml -Ddb_host="ХОСТ" -Ddb_name="ИМЯ БАЗЫ" -Ddb_username="ПОЛЬЗОВАТЕЛЬ" -Ddb_password="ПАРОЛЬ" -Ddb_root_username="СУПЕРПОЛЬЗОВАТЕЛЬ" -Ddb_root_password="ПАРОЛЬ"

##Production
phing build -f ./build/pro/build.xml -Ddb_host="ХОСТ" -Ddb_name="ИМЯ БАЗЫ" -Ddb_username="ПОЛЬЗОВАТЕЛЬ" -Ddb_password="ПАРОЛЬ" -Ddb_root_username="СУПЕРПОЛЬЗОВАТЕЛЬ" -Ddb_root_password="ПАРОЛЬ" -Dcookie_validation_key="КЛЮЧ"

#Тестирование приложения
- Чтобы назначить пользователю  роль "Админ" (или иную), следует выполнить команду:  
php yii rbac/assign
- Чтобы снять с пользователя роль "Админ" (или иную), следует выполнить команду:  
php yii rbac/revoke
- По желанию можно выполнить команду:  
php yii test/add-users  
Данный скрипт создаст трёх тестовых пользователей: админа (логин admin, пароль admin),
пользователя user1 (логин user1, пароль user1), пользователя user2 (логин user2, пароль user2).
