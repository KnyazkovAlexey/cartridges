<?php

namespace app\behaviors;

use app\helpers\StringHelper;
use yii\behaviors\AttributeBehavior;

/**
 * Поведение для преобразования дат в timestamp.
 * Это будет полезно, когда в БД надо сохранить timestamp, но в модель пришла дата в другом формате.
 *
 * Использование:
 *     public function behaviors()
 *     {
 *         return [
 *             StrtotimeBehavior::className(),
 *             'attributes' => [
 *                 ActiveRecord::EVENT_BEFORE_INSERT => 'myDate',
 *                 ActiveRecord::EVENT_BEFORE_UPDATE => 'myDate',
 *             ],
 *         ];
 *     }
 *
 * Class StrtotimeBehavior
 * @package app\behaviors
 */
class StrtotimeBehavior extends AttributeBehavior
{
    /**
     * {@inheritdoc}
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
            if (StringHelper::isDate($this->owner->{$this->attributes[$event->name]})) {
                return strtotime($this->owner->{$this->attributes[$event->name]});
            } elseif (StringHelper::isTimestamp($this->owner->{$this->attributes[$event->name]})) {
                return $this->owner->{$this->attributes[$event->name]};
            } else {
                return null;
            }
        }

        return parent::getValue($event);
    }
}
