<?php

use yii\db\Migration;

/**
 * Таблица "Картриджи"
 * Class m190223_172124_create_table_cartridges
 */
class m190223_172124_create_table_cartridges extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cartridges}}', [
            'id'          => $this->primaryKey(),
            'number'      => $this->string(50)->notNull()->unique()->comment('Номер'),
            'modelId'     => $this->integer()->comment('Модель'),
            'status'      => $this->string(50)->notNull()->comment('Статус'),
            'printerId'   => $this->integer()->comment('Принтер'),
            'purchasedAt' => $this->integer()->comment('Дата покупки'),
        ]);
        $this->addForeignKey('fk_cartridges_modelId', '{{%cartridges}}', 'modelId', '{{%cartridge_models}}', 'id',
            'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cartridges}}');
    }
}
