<?php

use yii\db\Migration;

/**
 * Таблица "Пользователи"
 * Class m190222_131230_create_table_users
 */
class m190222_131230_create_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id'          => $this->primaryKey(),
            'login'       => $this->string(50)->notNull()->unique()->comment('Логин'),
            'passwordHash'=> $this->string()->comment('Хеш пароля'),
            'authKey'     => $this->string()->comment('Ключ аутентификации'),
            'status'      => $this->string(50)->comment('Статус'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
