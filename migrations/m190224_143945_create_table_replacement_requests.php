<?php

use yii\db\Migration;

/**
 * Таблица "Заявки на замену картриджа"
 * Class m190224_143945_create_table_replacement_requests
 */
class m190224_143945_create_table_replacement_requests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%replacement_requests}}', [
            'id'        => $this->primaryKey()->comment('Номер'),
            'printerId' => $this->integer()->comment('Принтер'),
            'status'    => $this->string(50)->notNull()->comment('Статус'),
            'createdAt' => $this->integer()->comment('Дата создания'),
            'authorId'  => $this->integer()->comment('Автор'),
            'closedAt'  => $this->integer()->comment('Дата закрытия'),
        ]);
        $this->addForeignKey('fk_replacement_requests_printerId', '{{%replacement_requests}}', 'printerId', '{{%printers}}', 'id',
            'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_replacement_requests_authorId', '{{%replacement_requests}}', 'authorId', '{{%users}}', 'id',
            'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%replacement_requests}}');
    }
}
