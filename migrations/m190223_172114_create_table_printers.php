<?php

use yii\db\Migration;

/**
 * Таблица "Принтеры"
 * Class m190223_172114_create_table_printers
 */
class m190223_172114_create_table_printers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%printers}}', [
            'id'      => $this->primaryKey(),
            'number'  => $this->string(50)->notNull()->unique()->comment('Номер'),
            'modelId' => $this->integer()->comment('Модель'),
        ]);
        $this->addForeignKey('fk_printers_modelId', '{{%printers}}', 'modelId', '{{%printer_models}}', 'id',
            'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%printers}}');
    }
}
