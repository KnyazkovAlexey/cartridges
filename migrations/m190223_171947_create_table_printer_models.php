<?php

use yii\db\Migration;

/**
 * Таблица "Модели принтеров"
 * Class m190223_171947_create_table_printer_models
 */
class m190223_171947_create_table_printer_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%printer_models}}', [
            'id'    => $this->primaryKey(),
            'title' => $this->string(50)->notNull()->unique()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%printer_models}}');
    }
}
