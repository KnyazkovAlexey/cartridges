<?php

use yii\db\Migration;

/**
 * Промежуточная таблица "Совместимые модели принтеров и картриджей"
 * Class m190223_172047_create_junction_table_for_printer_models_and_cartridge_models
 */
class m190223_172047_create_junction_table_for_printer_models_and_cartridge_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lnk_printer_models_cartridge_models', [
            'printerModelId'   => $this->integer(),
            'cartridgeModelId' => $this->integer(),
            'PRIMARY KEY(printerModelId, cartridgeModelId)',
        ]);

        $this->createIndex(
            'idx-lnk_printer_models_cartridge_models-printerModelId',
            '{{%lnk_printer_models_cartridge_models}}',
            'printerModelId'
        );

        $this->addForeignKey(
            'fk-lnk_printer_models_cartridge_models-printerModelId',
            '{{%lnk_printer_models_cartridge_models}}',
            'printerModelId',
            '{{%printer_models}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-lnk_printer_models_cartridge_models-cartridgeModelId',
            '{{%lnk_printer_models_cartridge_models}}',
            'cartridgeModelId'
        );

        $this->addForeignKey(
            'fk-lnk_printer_models_cartridge_models-cartridgeModelId',
            '{{%lnk_printer_models_cartridge_models}}',
            'cartridgeModelId',
            '{{%cartridge_models}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lnk_printer_models_cartridge_models');
    }
}
