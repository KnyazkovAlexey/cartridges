<?php

use yii\db\Migration;

/**
 * Таблица "Модели картриджей"
 * Class m190223_172006_create_table_cartridge_models
 */
class m190223_172006_create_table_cartridge_models extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cartridge_models}}', [
            'id'    => $this->primaryKey(),
            'title' => $this->string(50)->notNull()->unique()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cartridge_models}}');
    }
}
