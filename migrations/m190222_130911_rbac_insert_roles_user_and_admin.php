<?php

use yii\db\Migration;
use app\rbac\Rbac;

/**
 * RBAC-роли "Пользователь" и "Админ"
 * Class m190222_130911_rbac_insert_roles_user_and_admin
 */
class m190222_130911_rbac_insert_roles_user_and_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $user = $auth->createRole(Rbac::ROLE_USER);
        $auth->add($user);

        $admin = $auth->createRole(Rbac::ROLE_ADMIN);
        $auth->add($admin);

        $auth->addChild($admin, $user);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $user = $auth->getRole(Rbac::ROLE_USER);
        $auth->remove($user);

        $admin = $auth->getRole(Rbac::ROLE_ADMIN);
        $auth->remove($admin);
    }
}
