<?php

use yii\db\Migration;
use app\rbac\rules\ManageClosedReplacementRequest;
use app\rbac\rules\ManageOwnReplacementRequest;
use app\rbac\Rbac;

/**
 * RBAC-разрешения для работы с сущностью "Заявка на замену картриджа"
 * Class m190225_110153_rbac_insert_permissions_for_replacement_request
 */
class m190225_110153_rbac_insert_permissions_for_replacement_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //Разрешение "Управление закрытой заявкой на замену картриджа"

        $rule = new ManageClosedReplacementRequest();
        $auth->add($rule);

        $manageClosedReplacementRequest = $auth->createPermission(Rbac::PERMISSION_MANAGE_CLOSED_REPLACEMENT_REQUEST);
        $manageClosedReplacementRequest->ruleName = $rule->name;
        $auth->add($manageClosedReplacementRequest);

        $user = $auth->getRole(Rbac::ROLE_USER);
        $auth->addChild($user, $manageClosedReplacementRequest);

        //Разрешение "Управление своей заявкой на замену картриджа"

        $rule = new ManageOwnReplacementRequest();
        $auth->add($rule);

        $manageOwnReplacementRequest = $auth->createPermission(Rbac::PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST);
        $manageOwnReplacementRequest->ruleName = $rule->name;
        $auth->add($manageOwnReplacementRequest);

        $user = $auth->getRole(Rbac::ROLE_USER);
        $auth->addChild($user, $manageOwnReplacementRequest);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        //Разрешение "Управление своей заявкой на замену картриджа"

        $manageOwnReplacementRequest = $auth->getPermission(Rbac::PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST);
        $auth->remove($manageOwnReplacementRequest);

        $rule = $auth->getRule(Rbac::RULE_MANAGE_OWN_REPLACEMENT_REQUEST);
        $auth->remove($rule);

        //Разрешение "Управление закрытой заявкой на замену картриджа"

        $manageClosedReplacementRequest = $auth->getPermission(Rbac::PERMISSION_MANAGE_CLOSED_REPLACEMENT_REQUEST);
        $auth->remove($manageClosedReplacementRequest);

        $rule = $auth->getRule(Rbac::RULE_MANAGE_CLOSED_REPLACEMENT_REQUEST);
        $auth->remove($rule);
    }
}
