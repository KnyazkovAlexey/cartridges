<?php

namespace app\helpers;

/**
 * Хелпер для работы со строками
 * Class StringHelper
 * @package app\helpers
 */
class StringHelper
{
    /**
     * Функция проверяет, является ли строка целым числом
     *
     * @param string $str
     * @return bool
     */
    public static function isInt($str)
    {
        return is_int($str) || preg_match('/^\d+$/', $str) === 1;
    }

    /**
     * Функция проверяет, является ли строка меткой времени
     *
     * @param string $str
     * @return bool
     */
    public static function isTimestamp($str)
    {
        return self::isInt($str) && strtotime(date('d-m-Y H:i:s', $str)) === (int)$str;
    }

    /**
     * Функция проверяет, является ли строка датой
     *
     * @param string $str
     * @return bool
     */
    public static function isDate($str)
    {
        return !self::isTimestamp($str) && strtotime($str) !== false;
    }
}