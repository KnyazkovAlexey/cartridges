<?php

namespace app\rbac;

/**
 * Класс хранит константы с наименованиями ролей, разрешений и правил RBAC
 *
 * Class Rbac
 * @package app\rbac
 */
class Rbac
{
    /**
     * Роль "Пользователь"
     */
    const ROLE_USER  = 'user';
    /**
     * Роль "Админ"
     */
    const ROLE_ADMIN = 'admin';

    /**
     * Разрешение "Управление закрытой заявкой на замену картриджа"
     */
    const PERMISSION_MANAGE_CLOSED_REPLACEMENT_REQUEST = 'manageClosedReplacementRequest';
    /**
     * Разрешение "Управление своей заявкой на замену картриджа"
     */
    const PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST = 'manageOwnReplacementRequest';

    /**
     * Правило для разрешения "Управление закрытой заявкой на замену картриджа"
     */
    const RULE_MANAGE_CLOSED_REPLACEMENT_REQUEST = 'manageClosedReplacementRequest';
    /**
     * Правило для разрешения "Управление своей заявкой на замену картриджа"
     */
    const RULE_MANAGE_OWN_REPLACEMENT_REQUEST = 'manageOwnReplacementRequest';
}
