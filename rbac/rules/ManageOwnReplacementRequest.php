<?php

namespace app\rbac\rules;

use app\models\replacementRequest\ReplacementRequest;
use yii\rbac\Rule;

/**
 * Правило для разрешения "Управление своей заявкой на замену картриджа"
 *
 * Class ManageClosedReplacementRequest
 * @package app\rbac\rules
 */
class ManageOwnReplacementRequest extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'manageOwnReplacementRequest';

    /**
     * Проверяем, что пользователь является автором данной заявки
     *
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return isset($params['replacementRequest']) && $params['replacementRequest']->authorId == $user;
    }
}
