<?php

namespace app\rbac\rules;

use app\models\replacementRequest\ReplacementRequest;
use yii\rbac\Rule;

/**
 * Правило для разрешения "Управление закрытой заявкой на замену картриджа"
 *
 * Class ManageClosedReplacementRequest
 * @package app\rbac\rules
 */
class ManageClosedReplacementRequest extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'manageClosedReplacementRequest';

    /**
     * Нельзя редактировать или удалять закрытые заявки
     *
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return isset($params['replacementRequest']) && $params['replacementRequest']->status != ReplacementRequest::STATUS_CLOSED;
    }
}
