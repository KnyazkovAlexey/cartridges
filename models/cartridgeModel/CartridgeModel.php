<?php

namespace app\models\cartridgeModel;

use app\models\cartridge\Cartridge;
use app\models\printerModel\PrinterModel;
use Yii;

/**
 * This is the model class for table "{{%cartridge_models}}".
 *
 * @property int $id
 * @property string $title Наименование
 *
 * @property Cartridge[] $cartridges Картриджи
 * @property PrinterModel[] $printerModels Совместимые модели принтеров
 */
class CartridgeModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cartridge_models}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 50],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartridges()
    {
        return $this->hasMany(Cartridge::className(), ['modelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrinterModels()
    {
        return $this->hasMany(PrinterModel::className(), ['id' => 'printerModelId'])->viaTable('{{%lnk_printer_models_cartridge_models}}', ['cartridgeModelId' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CartridgeModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CartridgeModelQuery(get_called_class());
    }
}
