<?php
namespace app\models;

use app\rbac\Rbac;
use Yii;
use yii\base\Model;

/**
 * Sign-up form
 */
class SignUpForm extends Model
{
    public $login;
    public $password;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string', 'max' => 50],
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'unique',
                'targetClass' => 'app\models\User',
            ],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login'      => Yii::t('app','Логин'),
            'password'   => Yii::t('app','Пароль'),
            'verifyCode' => Yii::t('app',''),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \Exception
     */
    public function signUp()
    {
        if ($this->validate()) {
            $user = new User();
            $user->login = $this->login;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $auth = Yii::$app->authManager;
                $auth->assign($auth->getRole(Rbac::ROLE_USER), $user->getId());
                return $user;
            }
        }

        return null;
    }
}
