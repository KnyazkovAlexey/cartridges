<?php

namespace app\models\printer;

use app\models\cartridge\Cartridge;
use app\models\printerModel\PrinterModel;
use Yii;

/**
 * This is the model class for table "{{%printers}}".
 *
 * @property int $id
 * @property string $number Номер
 * @property int $modelId Id модели принтера
 *
 * @property PrinterModel $model Модель принтера
 * @property Cartridge $cartridge Установленный картридж
 */
class Printer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%printers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'modelId'], 'required'],
            [['modelId'], 'integer'],
            [['number'], 'string', 'max' => 50],
            [['number'], 'unique'],
            [['modelId'], 'exist', 'skipOnError' => true, 'targetClass' => PrinterModel::className(), 'targetAttribute' => ['modelId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Номер'),
            'modelId' => Yii::t('app', 'Модель'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(PrinterModel::className(), ['id' => 'modelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartridge()
    {
        return $this->hasOne(Cartridge::className(), ['printerId' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PrinterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrinterQuery(get_called_class());
    }
}
