<?php

namespace app\models\printer;

use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Printer]].
 *
 * @see Printer
 */
class PrinterQuery extends \yii\db\ActiveQuery
{
    /**
     * Функция возвращает список моделей в виде 'id' => 'title'
     *
     * @param string $idAttribute
     * @param string $titleAttribute
     * @return array
     */
    public function asList($idAttribute = 'id', $titleAttribute = 'title')
    {
        return ArrayHelper::map($this->all(), $idAttribute, $titleAttribute);
    }

    /**
     * {@inheritdoc}
     * @return Printer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Printer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
