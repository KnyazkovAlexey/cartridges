<?php

namespace app\models\replacementRequest;

/**
 * This is the ActiveQuery class for [[ReplacementRequest]].
 *
 * @see ReplacementRequest
 */
class ReplacementRequestQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ReplacementRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ReplacementRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
