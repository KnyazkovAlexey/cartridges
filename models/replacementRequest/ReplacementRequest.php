<?php

namespace app\models\replacementRequest;

use app\models\printer\Printer;
use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%replacement_requests}}".
 *
 * @property int $id Номер
 * @property int $printerId Принтер
 * @property int $status Статус
 * @property int $createdAt Дата создания
 * @property int $authorId Автор
 * @property int $closedAt Дата закрытия
 *
 * @property Printer $printer
 */
class ReplacementRequest extends \yii\db\ActiveRecord
{
    /**
     * Статус "Открыта"
     */
    const STATUS_OPENED = 'opened';
    /**
     * Статус "Закрыта"
     */
    const STATUS_CLOSED = 'closed';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
                ],
            ],
            [
                'class'      => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['authorId'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%replacement_requests}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['printerId'], 'required'],
            ['status', 'default', 'value' => self::STATUS_OPENED],
            ['status', 'in', 'range' => array_keys(self::getStatusList())],
            [['printerId', 'createdAt', 'authorId', 'closedAt'], 'integer'],
            [['printerId'], 'exist', 'skipOnError' => true, 'targetClass' => Printer::className(), 'targetAttribute' => ['printerId' => 'id']],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер'),
            'printerId' => Yii::t('app', 'Принтер'),
            'status' => Yii::t('app', 'Статус'),
            'createdAt' => Yii::t('app', 'Дата создания'),
            'authorId' => Yii::t('app', 'Автор'),
            'closedAt' => Yii::t('app', 'Дата закрытия'),
        ];
    }

    /**
     * @return array Список всех возможных статусов
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_OPENED => Yii::t('app', 'Открыта'),
            self::STATUS_CLOSED => Yii::t('app', 'Закрыта'),
        ];
    }

    /**
     * @return string Читабельный заголовок статуса
     */
    public function getStatusLabel()
    {
        return self::getStatusList()[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrinter()
    {
        return $this->hasOne(Printer::className(), ['id' => 'printerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    /**
     * {@inheritdoc}
     * @return ReplacementRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReplacementRequestQuery(get_called_class());
    }
}
