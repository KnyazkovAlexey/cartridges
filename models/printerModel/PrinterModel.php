<?php

namespace app\models\printerModel;

use app\models\cartridgeModel\CartridgeModel;
use app\models\printer\Printer;
use Yii;

/**
 * This is the model class for table "{{%printer_models}}".
 *
 * @property int $id
 * @property string $title Наименование
 *
 * @property Printer[] $printers Принтеры
 * @property CartridgeModel[] $cartridgeModels Совместимые модели картриджей
 */
class PrinterModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%printer_models}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 50],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Наименование'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getCartridgeModels()
    {
        return $this->hasMany(CartridgeModel::className(), ['id' => 'cartridgeModelId'])->viaTable('{{%lnk_printer_models_cartridge_models}}', ['printerModelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrinters()
    {
        return $this->hasMany(Printer::className(), ['modelId' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PrinterModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrinterModelQuery(get_called_class());
    }
}
