<?php

namespace app\models\printerModel;

use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[PrinterModel]].
 *
 * @see PrinterModel
 */
class PrinterModelQuery extends \yii\db\ActiveQuery
{
    /**
     * Функция возвращает список моделей в виде 'id' => 'title'
     *
     * @param string $idAttribute
     * @param string $titleAttribute
     * @return array
     */
    public function asList($idAttribute = 'id', $titleAttribute = 'title')
    {
        return ArrayHelper::map($this->all(), $idAttribute, $titleAttribute);
    }

    /**
     * Функция возвращает список моделей в виде строки "title1, title2, ..."
     *
     * @param string $idAttribute
     * @param string $titleAttribute
     * @return array
     */
    public function asText($idAttribute = 'id', $titleAttribute = 'title')
    {
        return implode(', ', $this->asList($idAttribute, $titleAttribute));
    }

    /**
     * {@inheritdoc}
     * @return PrinterModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PrinterModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
