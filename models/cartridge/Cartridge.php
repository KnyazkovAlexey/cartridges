<?php

namespace app\models\cartridge;

use app\behaviors\StrtotimeBehavior;
use app\models\cartridgeModel\CartridgeModel;
use app\models\printer\Printer;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%cartridges}}".
 *
 * @property int $id
 * @property string $number Номер
 * @property int $modelId Id модели картриджа
 * @property string $status Статус
 * @property int $printerId Id принтера, куда установлен данный картридж
 * @property int $purchasedAt Дата покупки
 * @property string $fullTitle Полное наименование
 * @property string $statusLabel Читабельный заголовок статуса
 * @property array $statusList Список всех возможных статусов
 *
 * @property CartridgeModel $model Модель картриджа
 * @property Printer $printer Принтер, куда установлен данный картридж
 */
class Cartridge extends \yii\db\ActiveRecord
{
    /**
     * Статус "Полный"
     */
    const STATUS_FULL  = 'full';
    /**
     * Статус "Установлен в принтер"
     */
    const STATUS_IN_PRINTER  = 'in printer';
    /**
     * Статус "Пустой"
     */
    const STATUS_EMPTY  = 'empty';
    /**
     * Статус "Заправляется"
     */
    const STATUS_ON_REFILL  = 'on refill';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => StrtotimeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'purchasedAt',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'purchasedAt',
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cartridges}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'modelId'], 'required'],
            [['modelId', 'printerId'], 'integer'],
            [['purchasedAt'], 'safe'],
            [['number', 'status'], 'string', 'max' => 50],
            [['number'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_FULL],
            ['status', 'in', 'range' => array_keys(self::getStatusList())],
            [['modelId'], 'exist', 'skipOnError' => true, 'targetClass' => CartridgeModel::className(), 'targetAttribute' => ['modelId' => 'id']],
            [['printerId'], 'exist', 'skipOnError' => true, 'targetClass' => Printer::className(), 'targetAttribute' => ['printerId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'Номер'),
            'modelId' => Yii::t('app', 'Модель'),
            'status' => Yii::t('app', 'Статус'),
            'printerId' => Yii::t('app', 'Принтер'),
            'purchasedAt' => Yii::t('app', 'Дата покупки'),
        ];
    }

    /**
     * @return array Список всех возможных статусов
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_FULL       => Yii::t('app', 'Полный'),
            self::STATUS_IN_PRINTER => Yii::t('app', 'Установлен в принтер'),
            self::STATUS_EMPTY      => Yii::t('app', 'Пустой'),
            self::STATUS_ON_REFILL  => Yii::t('app', 'Заправляется'),
        ];
    }

    /**
     * @return string Читабельный заголовок статуса
     */
    public function getStatusLabel()
    {
        return self::getStatusList()[$this->status];
    }

    /**
     * @return string Полное наименование
     */
    public function getFullTitle()
    {
        return $this->number . ' ' . $this->model->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CartridgeModel::className(), ['id' => 'modelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrinter()
    {
        return $this->hasOne(Printer::className(), ['id' => 'printerId']);
    }

    /**
     * {@inheritdoc}
     * @return CartridgeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CartridgeQuery(get_called_class());
    }
}
