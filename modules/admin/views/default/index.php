<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app','Администрирование');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=$this->title?></h1>

        <p class="lead"><?=Yii::t('app', 'Административная часть приложения «Учет картриджей».')?></p>
    </div>

</div>
