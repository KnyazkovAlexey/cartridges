<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\rbac\Rbac;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app','Списки'), 'items' => [
                ['label' => Yii::t('app','Модели принтеров'), 'url' => ['/admin/printer-models']],
                ['label' => Yii::t('app','Модели картриджей'), 'url' => ['/admin/cartridge-models']],
                ['label' => Yii::t('app','Принтеры'), 'url' => ['/admin/printers']],
                ['label' => Yii::t('app','Картриджи'), 'url' => ['/admin/cartridges']],
                ['label' => Yii::t('app','Заявки'), 'url' => ['/admin/replacement-requests']],
            ]],
            ['label' => Yii::t('app','Формы'), 'items' => [
                ['label' => Yii::t('app','Замена'), 'url' => ['/admin/replacement']],
                ['label' => Yii::t('app','Отгрузка'), 'url' => ['/admin']],
                ['label' => Yii::t('app','Приемка'), 'url' => ['/admin']],
            ]],
            ['label' => Yii::t('app','Отчет'), 'url' => ['/admin']],
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app','Выйти (' . Yii::$app->user->identity->login . ')'),
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
