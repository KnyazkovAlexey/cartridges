<?php

namespace app\modules\admin;

use yii\filters\AccessControl;
use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::$app->errorHandler->errorAction = 'admin/default/error';

        // custom initialization code goes here
        $this->modules = [
            'printer-models' => [
                'class' => 'app\modules\admin\modules\printerModels\Module',
            ],
            'printers' => [
                'class' => 'app\modules\admin\modules\printers\Module',
            ],
            'cartridge-models' => [
                'class' => 'app\modules\admin\modules\cartridgeModels\Module',
            ],
            'cartridges' => [
                'class' => 'app\modules\admin\modules\cartridges\Module',
            ],
            'replacement-requests' => [
                'class' => 'app\modules\admin\modules\replacementRequests\Module',
            ],
            'replacement' => [
                'class' => 'app\modules\admin\modules\replacement\Module',
            ],
        ];
    }
}
