<?php

namespace app\modules\admin\modules\printers\models;

use Yii;

/**
 * Модель "Принтер" с дополнительными свойствами, необходимыми в данном модуле
 *
 * @inheritdoc
 *
 * @property string $compatibleCartridgeModelsAsText Совместимые модели картриджей в виде строки "title1, title2, ..."
 */
class Printer extends \app\models\printer\Printer
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'compatibleCartridgeModelsAsText' => Yii::t('app', 'Совместимые модели картриджей')
        ]);
    }

    /**
     * Совместимые модели картриджей в виде строки "title1, title2, ..."
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getCompatibleCartridgeModelsAsText()
    {
        return $this->model->getCartridgeModels()->asText() ?: null;
    }
}
