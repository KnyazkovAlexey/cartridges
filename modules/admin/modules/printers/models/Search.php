<?php

namespace app\modules\admin\modules\printers\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Search represents the model behind the search form of `app\models\printer\Printer`.
 */
class Search extends Printer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelId'], 'integer'],
            [['number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $query = Printer::find()->with('cartridge', 'model');

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'modelId' => $this->modelId,
            ]);

            $query->andFilterWhere(['like', 'number', $this->number]);
        }

        $dataProvider = new ArrayDataProvider([
            'key'       => 'id',
            'allModels' => $query->all(),
            'sort'      => [
                'attributes' => ['number', 'modelId', 'cartridge.fullTitle'],
            ],
        ]);

        return $dataProvider;
    }
}
