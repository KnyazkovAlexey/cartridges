<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\modules\printers\models\Search */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Принтеры');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'number',
                'label' => Yii::t('app', 'Номер'),
            ],
            [
                'attribute' => 'modelId',
                'value'     => 'model.title',
                'label'     => Yii::t('app', 'Модель'),
            ],
            [
                'attribute' => 'compatibleCartridgeModelsAsText',
                'label' => Yii::t('app', 'Совместимые модели картриджей'),
            ],
            [
                'attribute' => 'cartridge.fullTitle',
                'label' => Yii::t('app', 'Картридж'),
            ],
            //todo: Вывести дату установки картриджа

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
