<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\printer\Printer */
/* @var $printerModelList array */

$this->title = Yii::t('app', 'Добавить');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Принтеры'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'printerModelList' => $printerModelList,
    ]) ?>

</div>
