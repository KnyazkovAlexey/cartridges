<?php

namespace app\modules\admin\modules\printerModels\models;

use app\models\cartridgeModel\CartridgeModel;
use app\models\printerModel\PrinterModel;
use Yii;
use Exception;

/**
 * Модель для совместного сохранения модели принтера и совместимых с ней моделей картриджей
 *
 * Class PrinterModelForm
 * @package app\modules\admin\modules\printerModels\models;
 *
 * @property int[] $cartridgeModelIds Идентификаторы совместимых моделей картриджей
 * @property int[] $cartridgeModelsAsText Совместимые модели картриджей в виде строки "title1, title2, ..."
 */
class PrinterModelForm extends PrinterModel
{
    /**
     * @var int[] Идентификаторы совместимых моделей картриджей
     */
    private $cartridgeModelIds;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['cartridgeModelIds'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'cartridgeModelIds'     => Yii::t('app', 'Совместимые модели картриджей'),
            'cartridgeModelsAsText' => Yii::t('app', 'Совместимые модели картриджей')
        ]);
    }

    /**
     * Функция возвращает совместимые модели картриджей в виде строки "title1, title2, ..."
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getCartridgeModelsAsText()
    {
        return $this->getCartridgeModels()->orderBy('title')->asText() ?: null;
    }

    /**
     * Отдаём в форму идентификаторы совместимых моделей картриджей
     *
     * @return int[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getCartridgeModelIds()
    {
        return $this->getCartridgeModels()->asList('id', 'id');
    }

    /**
     * Принимаем из формы обновлённые идентификаторы совместимых моделей картриджей
     *
     * @param int[]|null $cartridgeModelIds
     */
    public function setCartridgeModelIds($cartridgeModelIds)
    {
        $this->cartridgeModelIds = $cartridgeModelIds;
    }

    /**
     * Совместное сохранение модели принтера и совместимых с ней моделей картриджей
     *
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws Exception;
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();

        if (!parent::save($runValidation, $attributeNames)) {
            $transaction->rollBack();
            return false;
        }

        try {
            $this->unlinkAll('cartridgeModels', true);
            if (!empty($this->cartridgeModelIds)) {
                foreach ($this->cartridgeModelIds as $cartridgeModelId) {
                    $this->link('cartridgeModels', CartridgeModel::findOne($cartridgeModelId));
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }
}

