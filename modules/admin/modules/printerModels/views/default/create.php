<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\printerModel\PrinterModel */
/* @var $cartridgeModelList array */

$this->title = Yii::t('app', 'Добавить');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели принтеров'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="printer-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cartridgeModelList' => $cartridgeModelList,
    ]) ?>

</div>
