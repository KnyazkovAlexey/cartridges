<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\printerModel\PrinterModel */
/* @var $form yii\widgets\ActiveForm */
/* @var $cartridgeModelList array */
?>

<div class="printer-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cartridgeModelIds')->dropDownList($cartridgeModelList, [
        'multiple' => 'multiple',
        'class'    => 'form-control',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
