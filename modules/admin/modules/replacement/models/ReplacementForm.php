<?php

namespace app\modules\admin\modules\replacement\models;

use app\models\cartridge\Cartridge;
use app\models\printer\Printer;
use app\models\printerModel\PrinterModel;
use app\models\replacementRequest\ReplacementRequest;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Форма замены картриджа
 *
 * Class ReplacementForm
 * @package app\modules\admin\modules\printerModels\models
 *
 * @property string $oldCartridgeNumber Номер снятого картриджа
 * @property string $newCartridgeNumber Номер установленного картриджа
 */
class ReplacementForm extends Model
{
    /**
     * @var string Номер снятого картриджа
     */
    public $oldCartridgeNumber;
    /**
     * @var string Номер установленного картриджа
     */
    public $newCartridgeNumber;
    /**
     * @var Cartridge Снятый картридж
     */
    private $_oldCartridge;
    /**
     * @var Cartridge Установленный картридж
     */
    private $_newCartridge;
    /**
     * @var Printer Принтер
     */
    private $_printer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldCartridgeNumber', 'newCartridgeNumber'], 'required'],
            [['oldCartridgeNumber', 'newCartridgeNumber'], 'safe'],
            [['oldCartridgeNumber'], 'validateOldCartridge'],
            [['newCartridgeNumber'], 'validateNewCartridge'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldCartridgeNumber' => Yii::t('app', 'Номер снятого картриджа'),
            'newCartridgeNumber' => Yii::t('app', 'Номер установленного картриджа')
        ];
    }

    /**
     * Заполняем вспомогательные поля
     *
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);

        if ($result) {
            $this->_oldCartridge = Cartridge::find()->with('printer.model')->where(['number' => $this->oldCartridgeNumber])->one();
            $this->_newCartridge = Cartridge::findOne(['number' => $this->newCartridgeNumber]);
            $this->_printer      = ArrayHelper::getValue($this->_oldCartridge, 'printer');
        }

        return $result;
    }

    /**
     * Валидация снятого картриджа
     *
     * @param $attribute
     * @param $params
     */
    public function validateOldCartridge($attribute, $params)
    {
        if (!$this->_oldCartridge) {
            $this->addError('oldCartridgeNumber', Yii::t('app', 'Картридж не найден'));
            return;
        }
        if ($this->_oldCartridge->status != Cartridge::STATUS_IN_PRINTER) {
            $this->addError('oldCartridgeNumber', Yii::t('app', 'Картридж уже снят'));
            return;
        }
    }

    /**
     * Валидация установленного картриджа
     *
     * @param $attribute
     * @param $params
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validateNewCartridge($attribute, $params)
    {
        if (!$this->_newCartridge) {
            $this->addError('newCartridgeNumber', Yii::t('app', 'Картридж не найден'));
            return false;
        }
        if ($this->_newCartridge->status != Cartridge::STATUS_FULL) {
            $this->addError('newCartridgeNumber', Yii::t('app', 'Картридж не полон'));
            return false;
        }
        if (!$this->_isCompatible($this->_newCartridge, $this->_printer->model)) {
            $this->addError('newCartridgeNumber', Yii::t('app', 'Картридж несовместим с моделью принтера'));
            return false;
        }
    }

    /**
     * Замена картриджа
     *
     * Логика следующая:
     * 1). Снять пустой картридж
     * 2). Установить полный картридж
     * 3). Закрыть заявку на замену картриджа, если она существует
     * 4). Создать заправку
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function replace()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->_revokeOldCartridge()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->_assignNewCartridge()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->_closeReplacementRequestIfExists()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->_addRefill()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();

        return true;
    }

    /**
     * Снятие пустого картриджа
     *
     * @return bool
     */
    private function _revokeOldCartridge()
    {
        $oldCartridge = $this->_oldCartridge;

        $oldCartridge->status = Cartridge::STATUS_EMPTY;
        $oldCartridge->printerId = null;

        return $oldCartridge->save();
    }

    /**
     * Установка полного картриджа
     *
     * @return bool
     */
    private function _assignNewCartridge()
    {
        $newCartridge = $this->_newCartridge;
        $printer      = $this->_printer;

        $newCartridge->status = Cartridge::STATUS_IN_PRINTER;
        $newCartridge->printerId = $printer->id;

        return $newCartridge->save();
    }

    /**
     * Закрытие заявки на замену картриджа, если она существует
     *
     * @return bool
     */
    private function _closeReplacementRequestIfExists()
    {
        $printer = $this->_printer;

        $replacementRequest = ReplacementRequest::findOne(['and', [
            'status' => ReplacementRequest::STATUS_OPENED,
            'printerId' => $printer->id
        ]]);

        if (!$replacementRequest) {
            return true;
        }

        $replacementRequest->status = ReplacementRequest::STATUS_CLOSED;
        $replacementRequest->closedAt = time();

        return $replacementRequest->save();
    }

    /**
     * Создание заправки
     *
     * @return bool
     */
    private function _addRefill()
    {
        return true;

        /**
         * todo: создать объект "Заправка", он понадобится для приёмки и отчёта:
         *
         * $refill = new Refill([
         *    'cartridgeId' => $this->_oldCartridge->id,
         *    'printerId'   => $this->_printer->id,
         *    'price'       => null,
         *    'date'        => null,
         *]);
         *
         * return $refill->save();
         */
    }

    /**
     * Проверка, что картридж совместим с моделью принтера
     *
     * @param Cartridge $cartridge
     * @param PrinterModel $printerModel
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function _isCompatible(Cartridge $cartridge, PrinterModel $printerModel)
    {
        return $printerModel->getCartridgeModels()->where(['id' => $cartridge->modelId])->exists();
    }
}

