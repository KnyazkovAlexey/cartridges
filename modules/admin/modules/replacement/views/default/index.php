<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\modules\replacement\models\ReplacementForm */

$this->title = Yii::t('app', 'Замена');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replacement-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'oldCartridgeNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newCartridgeNumber')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Заменить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
