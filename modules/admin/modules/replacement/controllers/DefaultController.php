<?php

namespace app\modules\admin\modules\replacement\controllers;

use app\modules\admin\modules\replacement\models\ReplacementForm;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `replacement` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $model = new ReplacementForm();

        if ($model->load(Yii::$app->request->post()) && $model->replace()) {
            Yii::$app->session->setFlash('success', 'Картридж успешно заменен');
            return $this->redirect(['/admin']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
