<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\cartridgeModel\CartridgeModel */
/* @var $form yii\widgets\ActiveForm */
/* @var $printerModelList array */
?>

<div class="cartridge-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'printerModelIds')->dropDownList($printerModelList, [
        'multiple' => 'multiple',
        'class'    => 'form-control',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
