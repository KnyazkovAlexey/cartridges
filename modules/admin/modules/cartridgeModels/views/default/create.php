<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartridgeModel\CartridgeModel */
/* @var $printerModelList array */

$this->title = Yii::t('app', 'Добавить');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели картриджей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartridge-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'printerModelList' => $printerModelList,
    ]) ?>

</div>
