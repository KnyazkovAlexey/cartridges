<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartridgeModel\CartridgeModel */
/* @var $printerModelList array */

$this->title = Yii::t('app', 'Редактировать');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели картриджей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartridge-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'printerModelList' => $printerModelList,
    ]) ?>

</div>
