<?php

namespace app\modules\admin\modules\cartridgeModels\models;

use app\models\cartridgeModel\CartridgeModel;
use app\models\printerModel\PrinterModel;
use Yii;
use Exception;

/**
 * Модель для совместного сохранения модели картриджа и совместимых с ней моделей принтеров
 *
 * Class CartridgeModelForm
 * @package app\modules\admin\modules\cartridgeModels\models;
 *
 * @property int[] $printerModelIds Идентификаторы совместимых моделей принтеров
 * @property int[] $printerModelsAsText Совместимые модели принтеров в виде строки "title1, title2, ..."
 */
class CartridgeModelForm extends CartridgeModel
{
    /**
     * @var int[] Идентификаторы совместимых моделей принтеров
     */
    private $printerModelIds;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['printerModelIds'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'printerModelIds'     => Yii::t('app', 'Совместимые модели принтеров'),
            'printerModelsAsText' => Yii::t('app', 'Совместимые модели принтеров')
        ]);
    }

    /**
     * Функция возвращает совместимые модели принтеров в виде строки "title1, title2, ..."
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrinterModelsAsText()
    {
        return $this->getPrinterModels()->orderBy('title')->asText() ?: null;
    }

    /**
     * Отдаём в форму идентификаторы совместимых моделей принтеров
     *
     * @return int[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getPrinterModelIds()
    {
        return $this->getPrinterModels()->asList('id', 'id');
    }

    /**
     * Принимаем из формы обновлённые идентификаторы совместимых моделей принтеров
     *
     * @param int[]|null $printerModelIds
     */
    public function setPrinterModelIds($printerModelIds)
    {
        $this->printerModelIds = $printerModelIds;
    }

    /**
     * Совместное сохранение модели картриджа и совместимых с ней моделей принтеров
     *
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     * @throws Exception;
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = Yii::$app->db->beginTransaction();

        if (!parent::save($runValidation, $attributeNames)) {
            $transaction->rollBack();
            return false;
        }

        try {
            $this->unlinkAll('printerModels', true);
            if (!empty($this->printerModelIds)) {
                foreach ($this->printerModelIds as $printerModelId) {
                    $this->link('printerModels', PrinterModel::findOne($printerModelId));
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }
}

