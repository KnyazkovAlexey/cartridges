<?php

namespace app\modules\admin\modules\cartridges\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Search represents the model behind the search form of `app\models\cartridge\Cartridge`.
 */
class Search extends Cartridge
{
    /**
     * @var int Совместимая модель принтеров
     */
    public $compatiblePrinterModelId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelId', 'printerId', 'compatiblePrinterModelId'], 'integer'],
            [['number', 'status', 'purchasedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $query = Cartridge::find()->with(['printer', 'model'])
            ->joinWith('model.printerModels AS compatiblePrinterModels',false);

        $this->load($params);

        if ($this->validate()) {
            $query->andFilterWhere([
                'printerId' => $this->printerId,
                'modelId' => $this->modelId,
                'compatiblePrinterModels.id' => $this->compatiblePrinterModelId,
            ]);

            $query->andFilterWhere(['like', 'number', $this->number])
                ->andFilterWhere(['like', 'status', $this->status]);

            if ($this->purchasedAt) {
                $query->andWhere(['>=', 'purchasedAt', strtotime($this->purchasedAt)])
                    ->andWhere(['<', 'purchasedAt', strtotime($this->purchasedAt . ' +1 day')]);
            }
        }

        $dataProvider = new ArrayDataProvider([
            'key'       => 'id',
            'allModels' => $query->all(),
            'sort'      => [
                'attributes' => ['number', 'modelId', 'status', 'printerId', 'purchasedAt'],
            ],
        ]);

        $dataProvider->sort->attributes['status'] = [
            'asc' => ['statusLabel' => SORT_ASC],
            'desc' => ['statusLabel' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['printerId'] = [
            'asc' => ['printer.number' => SORT_ASC],
            'desc' => ['printer.number' => SORT_DESC]
        ];

        return $dataProvider;
    }
}
