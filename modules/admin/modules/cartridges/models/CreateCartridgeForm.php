<?php

namespace app\modules\admin\modules\cartridges\models;

use Yii;

/**
 * Форма для создания картриджа (создана сугубо в целях тестирования)
 * Суть данной формы в том, чтобы при создании картриджа дать возможность руками назначить принтер картриджу
 *
 * Class CreateCartridgeForm
 * @package app\modules\admin\modules\cartridges\models
 */
class CreateCartridgeForm extends Cartridge
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['printerId'], 'validatePrinter'],
        ]);
    }

    /**
     * Проверка, что принтер свободен и совместим
     *
     * @param $attribute
     * @param $params
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePrinter($attribute, $params)
    {
        if (!$this->_isPrinterCompatible()) {
            $this->addError('printerId', Yii::t('app', 'Принтер несовместим'));
            return false;
        }
        if ($this->_isPrinterBusy()) {
            $this->addError('printerId', Yii::t('app', 'Принтер занят'));
            return false;
        }

        return true;
    }

    /**
     * Проверка, что выбранный принтер совместим с данным картриджем
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function _isPrinterCompatible()
    {
        return $this->printer->model->getCartridgeModels()->where(['id' => $this->modelId])->exists();
    }

    /**
     * Проверка, что выбранный принтер не занят другим картриджем
     *
     * @return bool
     */
    private function _isPrinterBusy()
    {
        return $this->printer->getCartridge()->exists();
    }
}
