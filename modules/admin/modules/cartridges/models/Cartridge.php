<?php

namespace app\modules\admin\modules\cartridges\models;

use Yii;

/**
 * Модель "Картридж" с дополнительными свойствами, необходимыми в данном модуле
 *
 * @inheritdoc
 *
 * @property string $compatiblePrinterModelsAsText Совместимые модели принтеров в виде строки "title1, title2, ..."
 */
class Cartridge extends \app\models\cartridge\Cartridge
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'compatiblePrinterModelsAsText' => Yii::t('app', 'Совместимые модели принтеров')
        ]);
    }

    /**
     * Совместимые модели принтеров в виде строки "title1, title2, ..."
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getCompatiblePrinterModelsAsText()
    {
        return $this->model->getPrinterModels()->asText() ?: null;
    }
}
