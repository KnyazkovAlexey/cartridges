<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartridge\Cartridge */
/* @var $cartridgeModelList array */

$this->title = Yii::t('app', 'Редактировать');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Картриджи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', $this->title);
?>
<div class="cartridge-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cartridgeModelList' => $cartridgeModelList,
    ]) ?>

</div>
