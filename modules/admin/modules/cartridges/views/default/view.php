<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\cartridge\Cartridge */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Картриджи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cartridge-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd.MM.Y',
        ],
        'attributes' => [
            'number',
            [
                'attribute' => 'modelId',
                'value'     => ArrayHelper::getValue($model, 'model.title'),
            ],
            'compatiblePrinterModelsAsText',
            [
                'attribute' => 'status',
                'value'     => $model->statusLabel,
            ],
            [
                'attribute' => 'printerId',
                'value'     => ArrayHelper::getValue($model, 'printer.number'),
            ],
            'purchasedAt:date',
        ],
    ]) ?>

</div>
