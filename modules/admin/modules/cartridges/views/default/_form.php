<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\cartridge\Cartridge */
/* @var $form yii\widgets\ActiveForm */
/* @var $cartridgeModelList array */
/* @var $printerList array */
?>

<div class="cartridge-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelId')->dropDownList($cartridgeModelList, ['prompt' => Yii::t('app', 'Не выбрано')]) ?>

    <?php if ($model->isNewRecord): ?>
        <?php //todo: сделать форму установки картриджа в пустой принтер, не давать это делать руками ?>
        <?php //В тестовых целях даём возможность руками назначить принтер картирджу:?>
        <?= $form->field($model, 'printerId')->dropDownList($printerList, ['prompt' => Yii::t('app', 'Не выбрано')]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'purchasedAt')->widget(DatePicker::className(), [
        'language'   => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
        'dateFormat' => 'dd.MM.yyyy',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
