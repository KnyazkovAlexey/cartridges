<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\cartridge\Cartridge */
/* @var $cartridgeModelList array */
/* @var $printerList array */

$this->title = Yii::t('app', 'Добавить');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Картриджи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartridge-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cartridgeModelList' => $cartridgeModelList,
        'printerList' => $printerList,
    ]) ?>

</div>
