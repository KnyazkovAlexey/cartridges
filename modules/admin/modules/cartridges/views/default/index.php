<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\modules\cartridges\models\Search */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $printerModelList array */
/* @var $cartridgeModelList array */
/* @var $printerList array */
/* @var $statusList array */

$this->title = Yii::t('app', 'Картриджи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cartridge-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd.MM.Y',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'number',
            [
                'attribute' => 'modelId',
                'value'     => 'model.title',
                'filter'    => $cartridgeModelList,
            ],
            [
                'attribute' => 'compatiblePrinterModelId',
                'value'     => 'compatiblePrinterModelsAsText',
                'label'     => Yii::t('app', 'Совместимые модели принтеров'),
                'filter'    => $printerModelList,
            ],
            [
                'attribute' => 'status',
                'value'     => 'statusLabel',
                'filter'    => $statusList,
            ],
            [
                'attribute' => 'printerId',
                'value'     => 'printer.number',
                'filter'    => $printerList,
            ],
            [
                'attribute' => 'purchasedAt',
                'format' => 'date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'purchasedAt',
                    'language' => 'ru',
                    'options' => [
                        'class' => 'form-control',
                    ],
                    'dateFormat' => 'dd.MM.yyyy',
                ]),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
