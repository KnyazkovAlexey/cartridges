<?php

namespace app\modules\admin\modules\cartridges\controllers;

use app\models\cartridgeModel\CartridgeModel;
use app\models\printer\Printer;
use app\models\printerModel\PrinterModel;
use app\modules\admin\modules\cartridges\models\CreateCartridgeForm;
use Yii;
use app\modules\admin\modules\cartridges\models\Cartridge;
use app\modules\admin\modules\cartridges\models\Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Cartridge model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cartridge models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'printerModelList' => PrinterModel::find()->orderBy('title')->asList(),
            'cartridgeModelList' => CartridgeModel::find()->orderBy('title')->asList(),
            'printerList'  => Printer::find()->orderBy('number')->asList('id', 'number'),
            'statusList'   => Cartridge::getStatusList(),
        ]);
    }

    /**
     * Displays a single Cartridge model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cartridge model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreateCartridgeForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'cartridgeModelList' => CartridgeModel::find()->orderBy('title')->asList(),
            'printerList'        => Printer::find()->orderBy('number')->asList('id', 'number'),
        ]);
    }

    /**
     * Updates an existing Cartridge model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'cartridgeModelList' => CartridgeModel::find()->orderBy('title')->asList(),
        ]);
    }

    /**
     * Deletes an existing Cartridge model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cartridge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cartridge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cartridge::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
