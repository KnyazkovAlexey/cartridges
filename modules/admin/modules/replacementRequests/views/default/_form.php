<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\replacementRequest\ReplacementRequest */
/* @var $form yii\widgets\ActiveForm */
/* @var $printerList array */
?>

<div class="replacement-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'printerId')->dropDownList($printerList, ['prompt' => Yii::t('app', 'Не выбрано')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
