<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\replacementRequest\ReplacementRequest */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="replacement-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'datetimeFormat' => 'php: d.m.Y H:i:s',
        ],
        'attributes' => [
            'id',
            'createdAt:datetime',
            [
                'attribute' => 'authorId',
                'value'     => ArrayHelper::getValue($model, 'author.login'),
            ],
            [
                'attribute' => 'printerId',
                'value'     => ArrayHelper::getValue($model, 'printer.number'),
            ],
            [
                'attribute' => 'status',
                'value'     => $model->statusLabel,
            ],
            'closedAt:date',
        ],
    ]) ?>

</div>
