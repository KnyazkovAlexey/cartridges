<?php

namespace app\modules\replacementRequests\models;

use app\rbac\Rbac;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\replacementRequest\ReplacementRequest;
use Yii;

/**
 * Search represents the model behind the search form of `app\models\replacementRequest\ReplacementRequest`.
 *
 * @property bool $showClosed Флаг "Показывать закрытые заявки"
 */
class Search extends ReplacementRequest
{
    /**
     * Чтение из сессии флага "Показывать закрытые заявки"
     * @return bool
     */
    public function getShowClosed()
    {
       return Yii::$app->session->get('replacementRequests_default_index_showClosed');
    }

    /**
     * Запись в сессию флага "Показывать закрытые заявки"
     * @param bool $value
     */
    public function setShowClosed($value)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->session->set('replacementRequests_default_index_showClosed', $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'printerId', 'createdAt', 'authorId', 'closedAt'], 'integer'],
            [['showClosed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'showClosed' => Yii::t('app', 'Показывать закрытые')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReplacementRequest::find()->joinWith(['printer AS printer']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['printerId'] = [
            'asc' => ['printer.number' => SORT_ASC],
            'desc' => ['printer.number' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['authorId' => Yii::$app->user->id]);

        if (!$this->showClosed) {
            $query->andWhere(['not', ['status' => ReplacementRequest::STATUS_CLOSED]]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'printerId' => $this->printerId,
            'createdAt' => $this->createdAt,
            'authorId' => $this->authorId,
            'closedAt' => $this->closedAt,
        ]);

        return $dataProvider;
    }
}
