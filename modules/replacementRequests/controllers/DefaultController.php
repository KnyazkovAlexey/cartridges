<?php

namespace app\modules\replacementRequests\controllers;

use app\models\printer\Printer;
use app\rbac\Rbac;
use Yii;
use app\models\replacementRequest\ReplacementRequest;
use app\modules\replacementRequests\models\Search;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for ReplacementRequest model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReplacementRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReplacementRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can(Rbac::PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST, ['replacementRequest' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Вы не можете управлять данной заявкой'));
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ReplacementRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReplacementRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'printerList' => Printer::find()->orderBy('number')->asList('id', 'number'),
        ]);
    }

    /**
     * Updates an existing ReplacementRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can(Rbac::PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST, ['replacementRequest' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Вы не можете управлять данной заявкой'));
        }
        if (!Yii::$app->user->can(Rbac::PERMISSION_MANAGE_CLOSED_REPLACEMENT_REQUEST, ['replacementRequest' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Нельзя редактировать закрытые заявки'));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'printerList' => Printer::find()->orderBy('number')->asList('id', 'number'),
        ]);
    }

    /**
     * Deletes an existing ReplacementRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can(Rbac::PERMISSION_MANAGE_OWN_REPLACEMENT_REQUEST, ['replacementRequest' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Вы не можете управлять данной заявкой'));
        }
        if (!Yii::$app->user->can(Rbac::PERMISSION_MANAGE_CLOSED_REPLACEMENT_REQUEST, ['replacementRequest' => $model])) {
            throw new ForbiddenHttpException(Yii::t('app', 'Нельзя удалять закрытые заявки'));
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReplacementRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReplacementRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReplacementRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
