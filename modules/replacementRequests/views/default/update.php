<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\replacementRequest\ReplacementRequest */
/* @var $printerList array */

$this->title = Yii::t('app', 'Редактировать');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replacement-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'printerList' => $printerList,
    ]) ?>

</div>
