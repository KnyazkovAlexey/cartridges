<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\replacementRequests\models\Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="replacement-request-search">

    <?php $form = ActiveForm::begin([
        'id' => 'search-form',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'showClosed')->checkbox([
        'onchange' => "$('#search-form').submit();",
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
