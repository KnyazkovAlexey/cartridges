<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\replacementRequest\ReplacementRequest */
/* @var $printerList array */

$this->title = Yii::t('app', 'Добавить');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявки'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replacement-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'printerList' => $printerList,
    ]) ?>

</div>
