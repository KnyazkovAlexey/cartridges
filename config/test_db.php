<?php

return [
    'class' => 'yii\db\Connection',
    'charset' => 'utf8',
    'dsn' => sprintf(
        'mysql:host=%s;dbname=%s',
        getenv('DB_HOST'),
        getenv('DB_NAME')
    ),
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
];
