<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\console\Exception;

/**
 * Группа команд, отвечающих за управление ролями и разрешениями RBAC
 */
class RbacController extends Controller
{
    /**
     * Adds role to user
     */
    public function actionAssign()
    {
        $login = $this->prompt('Login:', ['required' => true]);
        $user = $this->findModel($login);
        $roleName = $this->select('Role:', ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'));
        $authManager = Yii::$app->getAuthManager();
        $role = $authManager->getRole($roleName);
        $authManager->assign($role, $user->id);
        $this->stdout('Done!' . PHP_EOL);
    }

    /**
     * Removes role from user
     */
    public function actionRevoke()
    {
        $login = $this->prompt('Login:', ['required' => true]);
        $user = $this->findModel($login);
        $roleName = $this->select('Role:', ArrayHelper::merge(
            ['all' => 'All Roles'],
            ArrayHelper::map(Yii::$app->authManager->getRolesByUser($user->id), 'name', 'description'))
        );
        $authManager = Yii::$app->getAuthManager();
        if ($roleName == 'all') {
            $authManager->revokeAll($user->id);
        } else {
            $role = $authManager->getRole($roleName);
            $authManager->revoke($role, $user->id);
        }
        $this->stdout('Done!' . PHP_EOL);
    }

    /**
     * @param string $login
     * @throws \yii\console\Exception
     * @return User the loaded model
     */
    private function findModel($login)
    {
        if (!$model = User::findOne(['login' => $login])) {
            throw new Exception('User is not found');
        }
        return $model;
    }
}
