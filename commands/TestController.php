<?php

namespace app\commands;

use app\rbac\Rbac;
use yii\console\Controller;
use app\models\User;
use Yii;

/**
 * Группа команд для тестирования приложения
 */
class TestController extends Controller
{
    /**
     * Добавление в базу тестовых пользователей
     *
     * @throws \Exception
     */
    public function actionAddUsers()
    {
        $this->_createUser('admin', 'admin', Rbac::ROLE_ADMIN);
        $this->_createUser('user1', 'user1', Rbac::ROLE_USER);
        $this->_createUser('user2', 'user2', Rbac::ROLE_USER);

        $this->stdout('Done!' . PHP_EOL);
    }

    /**
     * Добавление в базу пользователя
     *
     * @param string $login
     * @param string $password
     * @param string $role
     * @throws \Exception
     */
    private function _createUser($login, $password, $role)
    {
        $auth = Yii::$app->authManager;

        $user = new User();
        $user->login = $login;
        $user->setPassword($password);
        $user->generateAuthKey();
        if ($user->save()) {
            $auth->assign($auth->getRole($role), $user->getId());
        }
    }
}
