<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app','Учет картриджей');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=$this->title?></h1>

        <p class="lead"><?=Yii::t('app', 'Приложение осуществляет контроль за заправкой картриджей в организации.')?></p>
    </div>

</div>
